# neuro-ai

Personal project to make an implementation of Neuroshima Hex board game and create a bot for it,   
probably trying to base on the methods used by AlphaZero.

# How to run
    1. Make sure you have all the required libraries. A superset of them is in the conda_requirements.txt.   
       You can use that file easily if you have conda installed. Then, from the terminal:
        * $ conda create -n new_environment_name --conda_requirements.txt
        * $ conda activate new_environment_name
    2. Make sure that neuro-ai folder is in the PYTHONPATH. Command in the next point will handle that on Linux/Mac
    3. In the neuro-ai/src folder, run command:
        * $ PYTHONPATH=<path_to_the_project> python main.py

# How to add your own army
    1. Make a new Python file in the data/armies folder. It must contain the army definition.

	An army definition should have a dictionary for each of the units in the army.   
	Then, it should have a list named "army", which is a list of all the tokens in the army.   
	It also needs to have "common_properties" dictionary, with at least the "colour" key   
	which contains the ANSI escape sequence defining the colour in which the army will be printed.  
	 See data/armies for examples.

# implementation notes
## Board
    * board is a Pandas DataFrame
    * A MultiIndex specifies the coordinates. Left upper corner is assumed to be (1, 1)
    * Coordinates are a bit weird because of the hexes. See game_engine.game_engine.create_board()
    * There are functions implemented to convert the Board back and forth between DataFrame and flat Series.   
      Flat Series will make a DataFrame of one game's history, and a concatenation of such dataframes will feed the ML.

## Unit and army
	* unit is a Pandas Series
	* units are defined by a dict structure. See "data/armies" folder
	* src.utils.army_creation.py has functions which convert the dictionaries into series,  
	  and lists of units into proper armies
	* src.utils.army_creation.py.get_armies() automatically converts all definitions  
	  in data/armies into armies inside one big dictionary.
### Unit and army: the weird stuff
	* An army definition should have a dictionary for each of the units in the army.   
	  Then, it should have a list named "army", which is a list of all the tokens in the army.  
	  It also needs to have "common_properties" dictionary,  
	  with at least the "colour" key which contains the ANSI escape sequence defining the colour in which the army will be printed.  
See data/armies for examples.
	* If a token is used in other way than by placing it on the board, e.g. it's a "move" immediate token,  
	  it requires a usage_function : function-reference part. t  
	  he referenced function should take, as of now, (board, unit_to_use, decisions_left) arguments   
	  and modify the board accordingly to it's purpose

# TODO:
	* bot making random decisions
	* bot using Monte Carlo Tree Search
	* if above works
	* if above works, expand the game with new units/armies and increase board size
	* if above works, expand the game with new features, like modules, medics, webs, action tokens etc.

# Issues:
    * a list of actions an unit might do in battle needs to be specified separately in several places of the code:
        - unit_dict_to_series in utils/army_creation.py
        - take_unit_actions in game_engine/unit_actions.py
        - get_field_ascii in gui/gui.py needs to be modified each time a new feature is introduced to the game, 
          so it print's the new tokens correctly
    * "Unit and army: the weird stuff" section contains things I feel deeply anxious about
      and would probably be better solved with OOP approach
    * Game frontend and backend aren't really separated as of now - the main game loop calls the functions
    from gui/gui.py, but all of them get created ad-hoc and aren't designed to make frontend development easy. 
    This will need to be solved.

# Open questions:
	-should the algorithm receive only "raw" board(unit names, directions and positions),  
	 or all the data("Hex (1,3) shoots SE with strength 3 in turn 4", etc.)