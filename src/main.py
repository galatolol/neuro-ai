import os
import sys
import pandas as pd
os.path.dirname(os.path.realpath(__file__))
os.chdir(os.path.dirname(os.getcwd()))

print(sys.path)
import src.game_engine.game_engine as game
from src.utils.army_creation import get_armies

armies = get_armies()
armies_dict = {"A" : armies["test_army_1"], 
               "B" : armies["test_army_2"]}

game.main_game_loop(armies_dict)