import pandas as pd
from src.game_engine import board_manipulation as manip

#Requires a list of actions
def take_unit_actions(board, initiative):
    possible_actions = ["shoot", "hit"]
    units_board = board.dropna(
            subset = [("initiative", "value")]
            )[board.initiative.value.dropna().str.contains(initiative)]
    for action in possible_actions:
        action_df = units_board[action].dropna(axis = 0, how = "all").\
                                        dropna(axis = 1, how = "all")
        if not action_df.empty:
            for direction in list(action_df.columns.values):
                directed_action_df = action_df[direction].dropna()
                for coords in list(directed_action_df.index.values):
                    if action is "hit":
                        hit(board,
                            coords[0], coords[1], direction,
                            directed_action_df.loc[coords],
                            board.loc[coords].id.allegiance)
                    if action is "shoot":
                        shoot(board,
                            coords[0], coords[1], direction,
                            directed_action_df.loc[coords],
                            board.loc[coords].id.allegiance)

def hit(board, source_x, source_y, direction, strength, allegiance):
    hex_to_hit = manip.get_neighbour(source_x, source_y, direction)
    if hex_to_hit in list(board.index.values):
        manip.deal_damage(board,
                          hex_to_hit,
                          manip.get_dmg_source_direction(direction),
                          strength,
                          allegiance,
                          "hit")

def shoot(board, source_x, source_y, direction, strength, allegiance):
    hex_to_hit = manip.get_neighbour(source_x, source_y, direction)
    unit_to_hit = manip.get_unit_to_dmg(board, hex_to_hit)
    #Anything on the board is either a unit or EMPTY_UNIT, therefore pd.Series
    while isinstance(unit_to_hit, pd.Series) and \
    (unit_to_hit.id["name"] is "empty" or \
    unit_to_hit["id", "allegiance"] == allegiance):
        hex_to_hit = manip.get_neighbour(hex_to_hit[0], 
                                         hex_to_hit[1], 
                                         direction)
        unit_to_hit = manip.get_unit_to_dmg(board, hex_to_hit)
    if isinstance(unit_to_hit, pd.Series):
        manip.deal_damage(board,
                          hex_to_hit,
                          manip.get_dmg_source_direction(direction),
                          strength,
                          allegiance,
                          "shoot")