import itertools
import pandas as pd
import numpy as np
import random
from data.comon_units import EMPTY_UNIT
from src.gui import gui
from src.game_engine import unit_actions as uactions

hex_width = 13
hex_height = 3

def main_game_loop(armies_dict):
    players = list(armies_dict.keys())
    winner = None
    board = create_board("small")
    turn = 1
    hands_dict = {p : [] for p in players}
    player_allegiances = {p : armies_dict[p][0]["id", "allegiance"] 
                            for p in players}
    #Scramble player's order and order of their armies/decks
    random.shuffle(players)
    for p in players:
        random.shuffle(armies_dict[p])
    #Game starts
    place_headquarters(players, armies_dict, board)
    while winner is None:
        for player in players:
            units_on_hand_limit = min([3, turn])
            gui.print_board(board, hex_width, hex_height)
            make_player_turn(board, 
                             player, 
                             armies_dict,
                             hands_dict,
                             units_on_hand_limit)
            winner = get_winner(board, player, armies_dict, player_allegiances)
            turn = turn + 1
    gui.print_board(board, hex_width, hex_height)
    if winner == "Tie":
        print("Everybody lost")
    else:
        print(winner + " is the winner")
    return winner

def get_winner(board, player, armies_dict, player_allegiances):
    other_player = (set(list(armies_dict.keys())) - set([player])).pop()
    hqs_left_count = board.loc[board.id.name == "hq"].shape[0]
    winner = None
    if hqs_left_count == 0:
        winner = "Tie"
    if hqs_left_count == 1:
        winner_allegiance = board.loc[board.id.name == "hq"].\
                            id.allegiance.values[0]
        winner = [p for p in player_allegiances
                  if player_allegiances[p] == winner_allegiance][0]                    
    elif len(armies_dict[other_player]) == 0 and hqs_left_count == 2:
        player_hq_hp = board[   (board.id.name == "hq")
                             &  (board.id.allegiance ==
                                 player_allegiances[player])]. \
                             HP.value.values[0]
        other_player_hq_hp = board[     (board.id.name == "hq") 
                                   &    (board.id.allegiance ==
                                         player_allegiances[other_player])]. \
                                   HP.value.values[0]
        if player_hq_hp == other_player_hq_hp:
            winner = "Tie"
        elif player_hq_hp > other_player_hq_hp:
            winner = player
        else:
            winner = other_player
    return winner
        
def place_headquarters(players, armies_dict, board):
      for player in players:
        gui.print_board(board, hex_width, hex_height)
        #extract_hq
        hq = [y for y in armies_dict[player] if y["id", "name"] == "hq"][0]
        hq_index = 0
        unit = armies_dict[player][hq_index]
        while unit["id", "name"] != "hq":
            hq_index = hq_index + 1
            unit = armies_dict[player][hq_index]
        hq = armies_dict[player].pop(hq_index)
        gui.print_player_hand(player, {player : [hq]}, hex_width, hex_height)
        print("Please, place your headquarter on the board.")
        #Place hq on board
        hq.nonML.usage_func(board, hq, 1)  

def make_player_turn(board, player, 
                     armies_dict, hands_dict,
                     units_on_hand_limit):
    get_new_units(player, 
                  armies_dict, hands_dict,
                  units_on_hand_limit)
    units_to_keep = []
    decisions_left = min([2, len(hands_dict[player])])
    while decisions_left > 0 and len(hands_dict[player]) > 0:
        unit_to_use = pick_unit_to_use(player, hands_dict)
        unit_was_used = False
        #K - keep, U - use, D - discard
        way_of_using = pick_action_for_unit()
        if way_of_using == "U":
            unit_was_used = unit_to_use.nonML.usage_func(board,
                                                         unit_to_use,
                                                         decisions_left)
            if not unit_was_used:
                hands_dict[player] = hands_dict[player] + [unit_to_use]
                decisions_left = decisions_left + 1
        elif way_of_using == "K":
            units_to_keep = units_to_keep + [unit_to_use]
        elif way_of_using == "D":
            decisions_left = decisions_left + 1
        gui.print_board(board, hex_width, hex_height)
        decisions_left = decisions_left - 1
        if check_for_battle(board, unit_to_use, unit_was_used, 
                            armies_dict, player, decisions_left):
            battle(board)
            break
    hands_dict[player] = units_to_keep


def check_for_battle(board, 
                     unit_to_use, 
                     unit_was_used,
                     armies_dict, 
                     player,
                     decisions_left):
    battle_happens = False
    other_player = (set(list(armies_dict.keys())) - set([player])).pop()
    if "empty" not in board.id.name.values \
        or unit_to_use.id.name == "battle" and unit_was_used \
        or len(armies_dict[other_player]) == 0 and decisions_left == 0:
        battle_happens = True
    return battle_happens

def pick_action_for_unit():
    return str(input("Do you wish to: \n \
                     K - keep token for future turns, or \n \
                     D - discard the token, or \n \
                     U - use the token?\n")).upper()

def pick_unit_to_use(player, hands_dict):
    gui.print_player_hand(player, hands_dict, hex_width, hex_height)
    possible_choices = [x+1 for x in list(range(len(hands_dict[player])))]
    unit_to_use_no = 0
    while not unit_to_use_no in possible_choices:
        unit_to_use_no = int(input("Pick which token you wish to use(" +\
                         str(possible_choices) + "): "))
    unit_to_use = hands_dict[player].pop(unit_to_use_no - 1)
    return unit_to_use

def get_new_units(player, 
                  armies_dict, hands_dict,
                  units_on_hand_limit):
    units_on_hand_count = len(hands_dict[player])
    units_left_count = len(armies_dict[player])
    #Draw new tokens
    while units_on_hand_count < units_on_hand_limit and units_left_count > 0:
        hands_dict[player] = hands_dict[player] + \
                        [armies_dict[player].pop(0)]
        units_on_hand_count = len(hands_dict[player])
        units_left_count = len(armies_dict[player])

def create_board(type):
    if type == "small":
        board_coords = \
                    list(itertools.product([1], [3, 5])) + \
                    list(itertools.product([2], [2, 4, 6,])) + \
                    list(itertools.product([3], [3, 5]))
    elif type == "big":
        board_coords = \
                list(itertools.product([1], [1, 3, 5, 7])) + \
                list(itertools.product([2], [0, 2, 4, 6, 8])) + \
                list(itertools.product([3], [1, 3, 5, 7])) + \
                list(itertools.product([0], [2, 4, 6])) + \
                list(itertools.product([4], [2, 4, 6]))
    elif type == "hand":
        board_coords = list(itertools.product([1, 3, 5], [1]))
        
    board = pd.DataFrame({coords : EMPTY_UNIT
                        for coords in board_coords})
    board = board.transpose()
    return board

def battle(board):
    print("BATTLE!")
    initiative_list = list(board.initiative.value.dropna())
    initiative_list = [int(elem) for y in initiative_list for elem in list(y)]
    initiative_list = list(set(initiative_list))
    initiative_list.sort(reverse = True)
    for init in initiative_list:
        uactions.take_unit_actions(board, str(init))
        clean_board(board)

def clean_board(board):
    killed_units = list(board[board.HP.value <= 0].index.values)
    if killed_units:
        for hex_coords in killed_units:
            kill_unit(board, hex_coords)

def kill_unit(board, hex_coords):
    board.loc[hex_coords] = EMPTY_UNIT

def board_df_to_series(board):
    return board.unstack(1).unstack(0)

def board_series_to_df(board_series, board_df):
    board = pd.DataFrame(data = board_series).\
                unstack().unstack().transpose()
    board.index = board.index.droplevel()
    rows_to_keep = board_df.index.values
    all_rows = board.index.values
    rows_to_drop = list(set(all_rows) - set(rows_to_keep))
    board.drop(rows_to_drop, inplace = True)
    board = board[board_df.columns.values]
    return board