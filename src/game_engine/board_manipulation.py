import numpy as np
import pandas as pd
import os

def get_neighbour(x, y, direction):
    if direction is "N":
        return (x, y - 2)
    if direction is "NE":
        return (x + 1, y - 1)
    if direction is "SE":
        return (x + 1, y + 1)
    if direction is "S":
        return (x, y + 2)
    if direction is "SW":
        return (x - 1, y + 1)
    if direction is "NW":
        return (x - 1, y - 1)
    
def get_dmg_source_direction(direction):
    if direction is "N":
        return "S"
    if direction is "NE":
        return "SW"
    if direction is "SE":
        return "NW"
    if direction is "S":
        return "N"
    if direction is "SW":
        return "NE"
    if direction is "NW":
        return "SE"
    
def get_unit_to_dmg(board, hex_to_hit):
    if hex_to_hit in list(board.index.values):
        return board.loc[hex_to_hit]
    else:
        return np.nan
    
def deal_damage(board, hex_to_hit, dmg_source_direction,
                strength, source_allegiance, reason):
    if source_allegiance != board.loc[hex_to_hit]["id", "allegiance"]:
        new_HP = board.loc[hex_to_hit]["HP", "value"] - strength
        board.loc[hex_to_hit]["HP", "value"] = new_HP
            
#Requires a list of actions
def rotate_unit(unit, new_north):
    rotated_unit = unit.copy()
    rotatable_actions = ["hit", "shoot"]
    numerised_directions = {"N"  : 0, "NE" : 1, "SE" : 2,
                            "S"  : 3, "SW" : 4, "NW" : 5
                            }
    symbolised_directions = {0 : "N", 1 : "NE", 2 : "SE",
                             3 : "S", 4 : "SW", 5 : "NW"}
    new_north_no = numerised_directions[new_north]
    for a in rotatable_actions:
        rotated_unit[a, "N"]  = unit[a,
                    symbolised_directions[(new_north_no + 0) % 6]]
        rotated_unit[a, "NE"] = unit[a,
                    symbolised_directions[(new_north_no - 1) % 6]]
        rotated_unit[a, "SE"] = unit[a,
                    symbolised_directions[(new_north_no - 2) % 6]]
        rotated_unit[a, "S"]  = unit[a,
                    symbolised_directions[(new_north_no - 3) % 6]]
        rotated_unit[a, "SW"] = unit[a,
                    symbolised_directions[(new_north_no - 4) % 6]]
        rotated_unit[a, "NW"] = unit[a,
                    symbolised_directions[(new_north_no - 5) % 6]]
    rotated_unit["id", "direction"] = new_north
    return rotated_unit