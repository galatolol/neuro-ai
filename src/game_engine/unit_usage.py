import pandas as pd
from src.game_engine import board_manipulation as manip

def place_unit_on_board(board, unit_to_use, decisions_left):
    #Battle happens but player needs to decide what to do with other units
    if decisions_left > 1 and "empty" not in board.id.name.values:
        print("Before initiating battle decide what to do with other tokens.")
        return False
    unit_coords = pick_coords_for_unit(board)
    unit_rotation = pick_unit_rotation()
    unit_to_use = manip.rotate_unit(unit_to_use, unit_rotation)
    board.loc[unit_coords] = unit_to_use
    return True

def pick_coords_for_unit(board):
    x = 0
    y = 0
    possible_xs = list(set([x for (x, y) in board[
                      board.id.name == "empty"].index.values]))
    
    while not x in possible_xs:
        x = int(input("Pick in which column you wish to place the unit(" +\
                             str(possible_xs) + "): "))
        
    possible_ys = list(board[board.id.name == "empty"].loc[x].index.values)  
    while not y in possible_ys:
        y = int(input("Pick in which row you wish to place the unit(" +\
                             str(possible_ys) + "): "))
    return((x, y))
    
def pick_unit_rotation():
    directions = ["N", "NE", "SE", "S", "SW", "NW"]
    unit_rotation = ""
    while not unit_rotation in directions:
        unit_rotation = str(input(
                "Type the direction you wish the unit to face(" + \
                str(directions) + ") ")).upper()
    return unit_rotation