import itertools
import numpy as np
import pandas as pd
import os
import importlib
import re
from src.game_engine import unit_usage

def get_armies():
    #Army is a file in the data.armies folder. This function transforms them
    list_of_armies = os.listdir(os.getcwd() + "/data/armies")
    regex = re.compile('.*\.py')
    list_of_armies = list(filter(regex.search, list_of_armies))
    #Cut the .py ending
    list_of_armies = [army[:-3] for army in list_of_armies]
    armies = {}
    for army_name in list_of_armies:
        army_definition = importlib.import_module(name = "data.armies." +
                                                      army_name,
                                                  package = "data.armies")
        army = army_definition.army
        common_props = army_definition.common_properties
        common_props["allegiance"] = army_name
        for prop in common_props.keys():
            for unit in army:
                unit[prop] = army_definition.common_properties[prop]
        army = [unit_dict_to_series(unit) for unit in army]
        armies[army_name] = army
    return armies

#Requires a list of actions
def unit_dict_to_series(unit_dict):
    #Dict is a convenient form to create an army.
    #pd.Series is the target structure, as board is a DataFrame
    fields = ["hit", "shoot"]
    directions = ["N", "NE", "SE", "S", "SW", "NW"]
    unit_param_ids = [x for x in itertools.product(fields, directions)]
    unit_param_ids = unit_param_ids + [("initiative", "value"),
                                       ("HP", "value"),
                                       ("colour", "value"),
                                       ("id", "allegiance"),
                                       ("nonML", "usage_func")]
    unit_series = pd.Series(index = pd.MultiIndex.\
                                    from_tuples(unit_param_ids))
    for action in unit_dict.keys():
        if isinstance(unit_dict[action], dict):
            for direction in unit_dict[action].keys():
                unit_series[action, direction] = unit_dict[action][direction]
                if not isinstance(unit_series[action, direction], str):
                    if np.isnan(unit_series[action, direction]):
                        unit_series[action, direction] = 0
        else:
            if action is "id":
                unit_series["id", "name"] = unit_dict["id"]
            if action is "allegiance":
                unit_series["id", "allegiance"] = unit_dict["allegiance"]
            if action in ["initiative", "HP", "colour"]:
                unit_series[action, "value"] = unit_dict[action]
            if action is "usage_func":
                unit_series["nonML", "usage_func"] = unit_dict[action]
    if np.isnan(unit_series["nonML", "usage_func"]):
        unit_series["nonML", "usage_func"] = unit_usage.place_unit_on_board
    unit_series["id", "direction"] = "N"
    return unit_series