import numpy as np
import pandas as pd
from collections import ChainMap
from data.comon_units import EMPTY_UNIT
import src.game_engine.game_engine as game
#"/"

def print_player_hand(player, hands_dict, hex_width, hex_height):
    hand_gui = game.create_board("hand")
    i = 0
    for ind in hand_gui.index.values:
        if i < len(hands_dict[player]):
            hand_gui.loc[ind] = hands_dict[player][i]
        else:
            hand_gui.loc[ind] = EMPTY_UNIT
        i = i + 1
    print_board(hand_gui, hex_width, hex_height)

def print_board(board, hex_width, hex_height):
    board = board.transpose()
    hexes_list = [get_field_ascii(start_x, start_y,
                              hex_width, hex_height,
                              board[start_x, start_y])
        for (start_x, start_y) in board.keys()]
    hexes_list = dict(ChainMap(*hexes_list))
    all_x = [x for (x, y) in list(hexes_list.keys())]
    all_y = [y for (x, y) in list(hexes_list.keys())]
    for y in range(1, max(all_y) + 1):
        for x in range(1, max(all_x) + 1):
            if (x, y) in hexes_list.keys():
                print(hexes_list[x, y], end = "")
            else:
                #Ensure that colour is set to default
                print("\033[0;0m ", end = "")
        print("")

#Requires a list of actions
def get_field_ascii(coord_x = 1, coord_y = 1,
                    x = 6, y = 2,
                    unit = {"name" : 'empty'}):
    #x = hex width, y - hex height
    #For explanation see docs/anatomia_hexa.png
    #A field in GUI is described by a dictionary with lists for each of symbols
    start_x = (x + y) * coord_x
    start_y = y * coord_y
    unit_ascii = {
            **get_field_borders   (unit, start_x, start_y, x, y),
            **get_shooting_ascii  (unit, x, y, start_x, start_y),
            **get_hit_ascii       (unit, x, y, start_x, start_y),
            **get_initiative_ascii(unit, x, y, start_x, start_y),
            **get_HP_ascii        (unit, x, y, start_x, start_y)
            }
    return unit_ascii



def get_field_borders(unit, start_x, start_y, x, y):
    #ensuring thet field borders are colour-neutral
    colour = "\033[0;0m"
    backslash_x = [bs_x + start_x for bs_x in
                   list(range(x + y + 1, x + 2 * y + 1)) + \
                   list(range(1, y + 1))]
    backslash_y = [bs_y + start_y for bs_y in list(range(2, 2 * y + 1 + 1))]
    coords_list_backslash = list(zip(backslash_x, backslash_y))
    coords_list_backslash = {(x, y) : colour + "\\" for
                             (x, y) in coords_list_backslash}
    slash_x = [s_x + start_x for s_x in
              list(range(y, 0, - 1)) + \
              list(range(x + 2 * y, x + y,-1))]
    slash_y = backslash_y
    coords_list_slash = list(zip(slash_x, slash_y))
    coords_list_slash = {(x, y) : colour + "/" for (x, y) in coords_list_slash}
    underscore_x = [ud_x + start_x for ud_x in
                    list(range(y + 1, y + x + 1)) + \
                    list(range(y + 1, y + x + 1))]
    underscore_y = [ud_y + start_y for ud_y in
                    list(np.repeat(1, x )) + \
                    list(np.repeat(2 * y + 1, x))]
    coords_list_underscore = list(zip(underscore_x, underscore_y))
    coords_list_underscore = {(x, y) : colour + "_" for
                              (x, y) in coords_list_underscore}
    coords_field_borders = {**coords_list_slash, **coords_list_backslash, \
                            **coords_list_underscore}
    return coords_field_borders

def get_shooting_ascii(unit, x, y, start_x, start_y):
    colour = unit["colour", "value"]
    if "shoot" not in unit.keys():
        return {}
    shooting_ascii = {}
    for direction in unit["shoot"].dropna().keys():
                    if direction is "N":
                        shooting_ascii[(start_x + y + x//2 + 1,
                                        start_y + 2)] = colour + "|"
                        shooting_ascii[(start_x + y + x//2 + 1,
                                        start_y + 3)] = colour + "|"
                    if direction is "S":
                        shooting_ascii[(start_x + y + x//2 + 1,
                                        start_y + 2 *y)] = colour + "|"
                        shooting_ascii[(start_x + y + x//2 + 1,
                                        start_y + 2 * y - 1)] = colour + "|"
                    if direction is "NE":
                        shooting_ascii[(start_x + x + 2*y - 2,
                                        start_y + y)] = colour + "/"
                        shooting_ascii[(start_x + x + 2*y - 3,
                                        start_y + y + 1)] = colour + "/"
                    if direction is "SW":
                        shooting_ascii[(start_x + y - 1,
                                        start_y + 2 * y - 1)] = colour + "/"
                        shooting_ascii[(start_x + y,
                                        start_y + 2 * y - 2)] = colour + "/"
                    if direction is "NW":
                        shooting_ascii[(start_x + y,
                                        start_y + y)] = colour + "\\"
                        shooting_ascii[(start_x + y + 1,
                                        start_y + y + 1)] = colour + "\\"
                    if direction is "SE":
                        shooting_ascii[(start_x + x + y + 1,
                                        start_y + 2 * y - 1)] = colour + "\\"
                        shooting_ascii[(start_x + x + y,
                                        start_y + 2 * y - 2)] = colour + "\\"
    return shooting_ascii

def get_hit_ascii(unit, x, y, start_x, start_y):
    colour = unit["colour", "value"]
    if "hit" not in unit.keys():
        return {}
    hit_ascii = {}
    for direction in unit["hit"].dropna().keys():
                if direction is "N":
                    hit_ascii[(start_x + y + x//2 + 1, 
                               start_y + 2)] = colour + "|"
                if direction is "S":
                    hit_ascii[(start_x + y + x//2 + 1, 
                               start_y + 2 *y)] = colour + "|"
                if direction is "NE":
                    hit_ascii[(start_x + x + 2*y - 2, 
                               start_y + y)] = colour + "/"
                if direction is "SW":
                    hit_ascii[(start_x + y - 1, 
                               start_y + 2 * y - 1)] = colour + "/"
                if direction is "NW":
                    hit_ascii[(start_x + y, 
                               start_y + y)] = colour + "\\"
                if direction is "SE":
                    hit_ascii[(start_x + x + y + 1, 
                               start_y + 2 * y - 1)] = colour + "\\"
    return hit_ascii

def get_initiative_ascii(unit, x, y, start_x, start_y):
    colour = unit["colour", "value"]
    if "initiative" not in unit.keys() \
        or pd.isnull(unit["initiative"]["value"]):
        return {}
    initiative_ascii = {}
    initiative_x = start_x + (x + 2 * y)//2
    initiative_y = start_y + (2 * y)//2 + 1
    init_list = list(unit["initiative"]['value'])
    initiative_ascii[(initiative_x, initiative_y)] = colour + "I"
    initiative_ascii[(initiative_x + 1, initiative_y)] = colour + ":"
    initiative_ascii[(initiative_x + 2, 
                      initiative_y)] = colour + init_list.pop(0)
    #Initiative is a string. Due to how print_board works, it's necessary
    #that every single sign get's it's own coordinate.
    i = 1
    while(init_list):
        initiative_ascii[(initiative_x + 2 + i,
                          initiative_y)] = colour + init_list.pop(0)
        i = i + 1
    return initiative_ascii

def get_HP_ascii(unit, x, y, start_x, start_y):
    colour = unit["colour", "value"]
    if "HP" not in unit.keys() or pd.isnull(unit["HP"]['value']):
        return {}
    hp_ascii = {}
    HP_x = start_x + (x + 2 * y)//2
    HP_y = start_y + (2 * y)//2 + 2
    hp_ascii[(HP_x, HP_y)] = colour + "H"
    hp_ascii[(HP_x + 1, HP_y)] = colour + "P"
    hp_ascii[(HP_x + 2, HP_y)] = colour + ":"
    hp_ascii[(HP_x + 3, HP_y)] = colour + str(int(unit["HP"]['value']))
    HP_length = len(str(int(unit["HP", "value"])))
    for i in range(1, HP_length):
        hp_ascii[(HP_x + 3 + i, HP_y)] = ""
    return hp_ascii

