shooter = {"id" : "shooter",
                               "shoot" : {"N" : 1},
                               "initiative" : "2",
                               "HP" : 1}

hq = {"id" : "hq",
                          "hit" : {"N" : 1,
                                     "NE" : 1,
                                     "SE" : 1,
                                     "S" : 1,
                                     "SW" : 1,
                                     "NW": 1},
                          "initiative": "0",
                          "HP" : 20}
                          
army = [hq, shooter, shooter, shooter, shooter]
common_properties = {"colour" : "\033[1;34m"}