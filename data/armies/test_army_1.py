hitter = {"id" : "hitter",
                              "hit" : {"N" : 1},
                              "initiative" :  "32",
                              "HP" : 1}

hq = {"id" : "hq",
                          "hit" : {"N" : 1,
                                     "NE" : 1,
                                     "SE" : 1,
                                     "S" : 1,
                                     "SW" : 1,
                                     "NW": 1},
                          "initiative": "0",
                          "HP" : 20}

army = [hq, hitter]#, hitter, hitter, hitter]
common_properties = {"colour" : "\033[1;31m"}